/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2022, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include <dashboard_example/gripper.h>
#include <kr2_program_api/internals/api/console.h>
#include <kr2_program_api/api_v1/bundles/arg_provider_xml.h>
#include <kr2_program_api/internals/api/program_interface_int.h>
#include <kr2_program_api/api_v1/cbun/xmlrpc/xmlrpc_server.h>

#include <string>

using namespace dashboard_example_cbun;

REGISTER_CLASS(dashboard_example_cbun::Gripper)


Gripper:: Gripper(boost::shared_ptr<kr2_program_api::ProgramInterface> a_api,
                                       const boost::property_tree::ptree &a_xml_bundle_node)
:   kr2_bundle_api::CustomDevice(a_api, a_xml_bundle_node)
{
    kr2_xmlrpc::XmlRpcServer server;

    class SetGripperWidthMethod : public kr2_xmlrpc::Method {
    public:

        Gripper* gripper_;

        SetGripperWidthMethod(Gripper* a_gripper)
        : gripper_(a_gripper)
        {}

        kr2_xmlrpc::Value execute(const kr2_xmlrpc::Params& a_params) {
            gripper_->gripper_width = a_params.getDouble(0);
            return kr2_xmlrpc::Value::Int(0);
        }
        
        
    };
    server.addMethod("setGripperWidth", boost::shared_ptr<SetGripperWidthMethod>(new SetGripperWidthMethod(this)));

    class GetGripperWidthMethod : public kr2_xmlrpc::Method {
    public:

        Gripper* gripper_;

        GetGripperWidthMethod(Gripper* a_gripper)
        : gripper_(a_gripper)
        {}

        kr2_xmlrpc::Value execute(const kr2_xmlrpc::Params& a_params) {
            return kr2_xmlrpc::Value::Double(gripper_->gripper_width);
        }
     
    
    };
    server.addMethod("getGripperWidth", boost::shared_ptr<GetGripperWidthMethod>(new GetGripperWidthMethod(this)));

}


int Gripper::onCreate()
{
    return 0;
}

int Gripper::onDestroy()
{
    onDeactivate();
    return 0;
}

int Gripper::onBind()
{    
    if (!activation_tree_) {
        CLOG_ERR("[CBUN/sample] Activation params are not available");
        return -1;
    }
    
    if (!processActivationParams(*activation_tree_)) {
        CLOG_ERR("[CBUN/sample] Invalid activation params");
        return -2;
    }

    return 0;
}

int Gripper::onUnbind()
{
    return 0;
}


CBUN_PCALL Gripper::onActivate(const boost::property_tree::ptree &a_param_tree)
{
    if (!processActivationParams(a_param_tree)) {
        CBUN_PCALL_RET_ERROR(-1, "Invalid activation params");
    }
    
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL Gripper::onDeactivate()
{
    CBUN_PCALL_RET_OK;
}


bool Gripper::processActivationParams(const boost::property_tree::ptree &a_param_tree)
{
    kr2_bundle_api::ArgProviderXml arg_provider(a_param_tree);
    if (arg_provider.getArgCount() != 0) {
        CLOG_ERR("[CBUN/GripperRG] Failed to process activation params: Unexpected param count=" << arg_provider.getArgCount());
        return false;
    }
    return true;
}


CBUN_PCALL Gripper::setGripperWidth(float a_width)
{
    CBUN_PCALL_RET_OK;
}