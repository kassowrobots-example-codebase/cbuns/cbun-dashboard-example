package com.kassowrobots.dashboardexamplecbun

/*
 * UserInputEvent is a base class for all UI input events, ie. all events to be triggered on user
 * interaction. Each input event is represented by a single subclass of UserInputEvent. It serves
 * for the View -> ViewModel communication. Once the user interacts with the UI, corresponding
 * event is sent from the view to the view model.
 */
sealed class UserInputEvent {

    // Event to be sent when the user moves the slider's thumb.
    data class ValueChanged(val value: Double) : UserInputEvent()

    // Event to be sent when the user puts up finger from the slider.
    object ValueChangeFinished : UserInputEvent()

}
