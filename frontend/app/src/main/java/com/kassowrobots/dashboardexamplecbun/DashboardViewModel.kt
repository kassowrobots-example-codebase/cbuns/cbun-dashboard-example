package com.kassowrobots.dashboardexamplecbun

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kassowrobots.api.robot.xmlrpc.Fault
import com.kassowrobots.api.robot.xmlrpc.Params
import com.kassowrobots.api.robot.xmlrpc.XmlRpcClient
import com.kassowrobots.api.robot.xmlrpc.values.Value
import com.kassowrobots.api.robot.xmlrpc.values.ValueArray
import com.kassowrobots.api.robot.xmlrpc.values.ValueDouble
import com.kassowrobots.api.util.KRLog
import java.lang.RuntimeException

class DashboardViewModel(
    private val xmlClient: XmlRpcClient
): ViewModel() {
    // Mutable OptionsState to be used for the UI update. The state itself has no access modifier,
    // ie. it is public by default (in order to be accessible from the view). But it implements
    // the property setter with private access modifier to restrict the state modification from the
    // outside of this class.
    var state by mutableStateOf(
        OptionsState(
            sourceVal = 0.5
        )
    )
        private set

    init{
        // get current configuration and update UI right after creation
        getGripperWidth()
    }

    /*
     * This method processes the user interaction events and provides the appropriate response. Each
     * interaction event is represented by a single subclass of UserInputEvent.
     */
    fun onUserInput(event: UserInputEvent) {
        when (event) {

            // If the source value has been changed, the new UI state is created as a copy of the
            // former state, but with the new sourceVal element.
            is UserInputEvent.ValueChanged -> updateState(event.value)

            // If the source value update was finished, the actual configuration is updated.
            is UserInputEvent.ValueChangeFinished -> requestGripperWidthUpdate()
        }
    }

    /*
     * The new UI state is created as a copy of the former state, but with the new sourceVal element.
     */
    private fun updateState(value: Double){
        state = state.copy(sourceVal = value)
    }

    /*
     * Sends update gripper width XML-RPC request to the backend.
     */
    private fun requestGripperWidthUpdate() {
        try {
            // new instance for parameters sent
            val params = Params()

            // adds gripper width as double
            params.add(ValueDouble(state.sourceVal))

            //executes the method using xmlRpc service
            xmlRpc("setGripperWidth", params)

        } catch (e: Fault) {
            KRLog.e("Robot", "Failed to set configuration: " + e.description)
        }
    }

    /*
     * Returns actual gripper width based on the XML-RPC response from the backend.
     */
    private fun getGripperWidth() {
        try {
            // executes the method using xmlRpc service
            val retVal = xmlRpc("getGripperWidth", Params())

            // processing returned parameters
            val value = (retVal as ValueDouble).double

            // UI update
            updateState(value)

        } catch (e: Fault) {
            KRLog.e("Robot", "Failed to get configuration: " + e.description)
        } catch (e: RuntimeException) {
            // this is thrown when running the dashboard in emulator
            KRLog.e("Robot", "Failed to get configuration: $e")
        }
    }

    /*
     * xmlClient execute method
    */
    @Synchronized
    @Throws(Fault::class)
    private fun xmlRpc(aMethodName: String, aParams: Params): Value {
        return xmlClient.execute(aMethodName, aParams, 2.0)
    }

    /*
     * ViewModelFactory is responsible for constructing of a ViewModel. In this tutorial, the custom
     * ViewModelFactory is defined in order to pass custom data (selected command options) into the
     * ViewModel.
     */
    class MyViewModelFactory(private val deviceName: String) : ViewModelProvider.NewInstanceFactory() {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T {

            // initializing new xmlClient for the ViewModel with the provided device name
            val xmlClient = XmlRpcClient(deviceName)

            return DashboardViewModel(xmlClient) as T
        }
    }
}