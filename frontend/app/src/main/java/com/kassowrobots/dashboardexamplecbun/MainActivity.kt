package com.kassowrobots.dashboardexamplecbun

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat

// MainActivity class for emulator testing
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFullscreen()
        setContentView(R.layout.activity_main)

        // Dashboard fragment is always launched with the unique device name argument.
        // This argument identifies the specific device instance that was used to launch the dashboard.
        // Such information can be utilised to the proper addressing of C++ backend via XML-RPC
        // (for example when 2 grippers are attached to the robot).
        val args = Bundle()
        args.putString("device_name", "test_name")
        val fragment = DashboardFragment()
        fragment.arguments = args

        //fragment transaction
        supportFragmentManager
            .beginTransaction()
            .add(R.id.container, fragment)
            .commit()
    }

    private fun setFullscreen() {
        window?.run {
            WindowCompat.setDecorFitsSystemWindows(this, false)
        }
        WindowInsetsControllerCompat(window, window.decorView).apply {
            hide(WindowInsetsCompat.Type.navigationBars())
        }
    }

}