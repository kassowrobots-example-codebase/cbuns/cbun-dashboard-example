package com.kassowrobots.dashboardexamplecbun

/*
 * OptionsState represents the state of the UI, ie. all that should be visible to the user. It
 * serves for the ViewModel -> View communication. Once the state data are updated, also the views
 * that access these data are updated. In this tutorial the OptionsState is used to provide the
 * feedback of source value.
 */
data class OptionsState(
    val sourceVal: Double = 0.0     // source value (to be used with the slider and text output)
)