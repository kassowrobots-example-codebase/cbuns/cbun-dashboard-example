package com.kassowrobots.dashboardexamplecbun

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.modifier.modifierLocalConsumer
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.graphics.toColorInt
import androidx.lifecycle.viewmodel.compose.viewModel
import com.kassowrobots.api.app.fragment.KRFragment

/*
 * DashboardFragment represents the dashboard Panel UI. It is simply an entry point
 * of CBunX application to be launched within the Teach Pendant host app once the user opens options
 * of this custom command (the command itself is defined via bundle.xml). The fragment is responsible
 * for the initialization of the UI view. In this tutorial, the Jetpack Compose is used to build the
 * UI view.
 */
class DashboardFragment : KRFragment(){
    // Function onCreateView is responsible for the UI initialization of this fragment.
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // ComposeView is an AndroidView that can host Jetpack Compose UI content. In this tutorial
        // the ComposeView represents the root view of this Options Panel fragment.
        return ComposeView(requireContext()).apply {
            // Function setViewCompositionStrategy is used to tell the compose to dispose itself
            // once the fragment is destroyed.
            setViewCompositionStrategy(
                ViewCompositionStrategy.DisposeOnLifecycleDestroyed(viewLifecycleOwner)
            )
            // Function setContent sets the Jetpack Compose UI content for this ComposeView.
            setContent {

                // ViewModel is responsible for processing user input events within this fragment's
                // view and providing the view update. In this tutorial, the ViewModel is constructed
                // by using custom ViewModel Factory in order to pass the custom data (obtained from
                // the passed arguments) into the ViewModel's constructor.
                val viewModel = viewModel<DashboardViewModel>(factory = DashboardViewModel.MyViewModelFactory(
                    requireArguments().getString("device_name")!!)
                )

                // Surface is a composable that allows to specify the 'surface' parameters, such as
                // background color, elevation, clip or background shape. In this tutorial, the
                // Surface is used to modify the background color of this Options Panel fragment.
                Surface(color = Color("#FFE2E6F0".toColorInt())) {

                    // Column is a composable layout that places its children in a vertical sequence.
                    // In this tutorial, the modifier concept is used to define the padding, ie. the
                    // outer offset to be used on the content of the column. Modifier is simply an
                    // implementation of decorator pattern that can be used to modify the composable
                    // that it is applied to.
                    Column(modifier = Modifier.padding(20.dp).fillMaxHeight()) {

                        // Spacer represents an empty space layout. In this case it creates vertical
                        // divider between the Heading1 and Heading2 with size of 20 dp.
                        Spacer(modifier = Modifier.height(20.dp))


                        // Another subheading.
                        Heading2(text="GRIPPER WIDTH")

                        // Row is a composable layout that places its children in a horizontal sequence.
                        Row {

                            // Slider is a composable that allows the user to set Float value within
                            // the slider's range. In this tutorial, the slider is used to define
                            // the source value within the slider's default range (0..1).
                            Slider(
                                // Value is updated on each recomposition, ie. on each viewModel.state
                                // update.
                                value = viewModel.state.sourceVal.toFloat(),
                                // onValueChange is called when the user moves the slider's thumb.
                                onValueChange = {
                                    viewModel.onUserInput(UserInputEvent.ValueChanged(it.toDouble()))
                                },
                                // onValueChangeFinished is called when user takes up the finger
                                // from the slider's thumb.
                                onValueChangeFinished = {
                                    viewModel.onUserInput(UserInputEvent.ValueChangeFinished)
                                },
                                // Weight 1f means that the slider occupies all available space, ie.
                                // all space that was left after composition of Spacer and Text bellow.
                                modifier = Modifier.weight(1f)
                            )

                            // This time, the space is used as a horizontal divider, since it is
                            // placed into a row and its width is specified.
                            Spacer(modifier = Modifier.width(20.dp))

                            // Text is a composable element that displays the text. In this case it
                            // is used to show the current source value.
                            Text(
                                // The actual text is obtained from the viewModel.state - note that
                                // the same sourceVal is also used for the slider output.
                                text="" + viewModel.state.sourceVal,

                                // The number of lines is limited to 1.
                                maxLines = 1,

                                // In case the content is too long, the ellipsis is used.
                                overflow = TextOverflow.Ellipsis,

                                // The text width is set to 80 dp and it is centered vertically
                                // within the row.
                                modifier = Modifier
                                    .width(80.dp)
                                    .align(alignment = Alignment.CenterVertically)
                            )
                        }

                    }
                }
            }
        }
    }


    /*
     * Custom composable that represents a subheading. It simply wraps the Jetpack Compose Text and
     * define some of its parameters, such as font size and font color. The text value and optional
     * modifier are passed as Heading2 arguments.
     */
    @Composable
    fun Heading2(text: String, modifier: Modifier = Modifier) {
        Text(
            text=text,
            fontSize = 14.sp,
            fontWeight = FontWeight.Bold,
            color = Color("#FF393B3F".toColorInt()),
            modifier = modifier
        )
    }

}